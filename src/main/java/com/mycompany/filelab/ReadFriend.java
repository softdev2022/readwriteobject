/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filelab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class ReadFriend {
    public static void main(String[] args) {
        FileInputStream fis = null;
        try {
            Friend friend01 = null;
            Friend friend02 = null;
            Friend friend03 = null;
            File file = new File("friends.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            friend01 = (Friend)ois.readObject();
            friend02 = (Friend)ois.readObject();
            friend03 = (Friend)ois.readObject();
            System.out.println(friend01);
            System.out.println(friend02);
            System.out.println(friend03);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
